package com.afpa.cda.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;

import com.afpa.cda.dto.UserDto;

@WebServlet(urlPatterns = {"/login.do"})
public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String login = req.getParameter("login");
		String pwd = req.getParameter("pwd");
		if (login == null || pwd == null || !login.startsWith("admin") || !pwd.startsWith("pwd")) {
			req.setAttribute("msg", "mauvais identifiant");
			getServletContext().getRequestDispatcher("/login.jsp").forward(req, resp);
		
		} else {
			//UserDto userEnCours = new UserDto(login, "nom");
			HttpSession session = req.getSession();//Cr�ation d'un espace m�moire
			session.setAttribute("personne", login);//Ajout du login (qui est de type String) dans la session(ou espace m�moire) avec pour identifiant "personne" qui sera � r�cup�rer dans 
			getServletContext().getRequestDispatcher("/list.jsp").forward(req, resp);
			
		}
	}	
}
