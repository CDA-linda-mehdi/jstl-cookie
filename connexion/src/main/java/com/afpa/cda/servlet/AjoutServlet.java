package com.afpa.cda.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.afpa.cda.dto.ProduitDto;
import com.afpa.cda.dto.UserDto;

@WebServlet(urlPatterns = { "/ajout.do" })
public class AjoutServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession(); //Cr�ation d'un espace m�moire
		String userEnCours = (String) session.getAttribute("personne"); //R�cup�ration des infos de session de connexion de 'internaute depuis LoginServlet
		if (userEnCours == null) { //Si espace m�moire = null signifie qu'il n'y a rien dans l'espace m�moire donc pas de connexion de l'internaute
			resp.sendRedirect("login.do"); //Alors redirection vers la page de connexion
		} else { //Sinon forward vers la page d'ajout de produits
			getServletContext().getRequestDispatcher("/ajout.jsp").forward(req, resp);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String label = req.getParameter("label");
		String prix = req.getParameter("prix");
		ProduitDto p = new ProduitDto(label, Integer.parseInt(prix));
		ListServlet.produits.add(p);
		getServletContext().getRequestDispatcher("/list.do").forward(req, resp);
	}
	
}
