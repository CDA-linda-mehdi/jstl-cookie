package com.afpa.cda.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.afpa.cda.dto.ProduitDto;

@WebServlet(urlPatterns = { "/index.html", "/list.do" })
public class ListServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static List<ProduitDto> produits = new ArrayList<ProduitDto>();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//int nbreProduit = new Random().nextInt(10)+5;
//		for (int i = 0; i < produits.size(); i++) {
//			produits.add(new ProduitDto("label", + i * 2));
//		}
		req.setAttribute("produits", produits);
		getServletContext().getRequestDispatcher("/list.jsp").forward(req, resp);
		
	}
	
}
