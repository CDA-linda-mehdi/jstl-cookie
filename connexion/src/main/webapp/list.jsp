<%@include file="header.jsp"%>
<style>
#sailorTableArea{
    max-height: 300px;
    overflow-x: auto;
    overflow-y: auto;
}
#sailorTable{
    white-space: normal;
}
tbody {
    display:block;
    height:200px;
    overflow:auto;
}
thead, tbody tr {
    display:table;
    width:100%;
    table-layout:fixed;
}
</style>

<%-- affichage de login de connexion (gr�ce � l'identifiant "personne" dans LoginServlet) si internaute connect� --%>

<div class="table-responsive" id="sailorTableArea">
	<table id="sailorTable" class="table table-striped table-bordered"
		width="100%">

		<thead>
			<tr>
				<th>Label</th>
				<th>Prix</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${ produits }" var="produit" varStatus="status">
				<tr>
					<td><c:out value="${ produit.label }" /></td>
					<td><c:out value="${ produit.prix }" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>

<!-- <table> -->
<!-- 	<tr> -->
<!-- 		<th>Label</th> -->
<!-- 		<th>Prix</th> -->
<!-- 	</tr> -->
<!-- 	<tbody> -->

<%-- 		<c:forEach items="${ produits }" var="produit" varStatus="status"> --%>
<!-- 			<tr> -->
<%-- 				<td><c:out value="${ produit.label }" /></td> --%>
<%-- 				<td><c:out value="${ produit.prix }" /></td> --%>
<!-- 			</tr> -->
<%-- 		</c:forEach> --%>

<!-- 	</tbody> -->

<!-- </table> -->


<%@include file="footer.jsp"%>