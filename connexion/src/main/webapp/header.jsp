<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Fruits et légumes</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">login : ${sessionScope.personne }</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a href="ajout.do"><button class="btn btn-outline-success my-2 my-sm-0" type="button">Ajouter produit</button></a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <a href="deco.do"><button class="btn btn-outline-success my-2 my-sm-0" type="button">Déconnexion</button></a>
    </form>
  </div>
</nav>
<br /><br />